---
title: Week 3
date: 2019-02-11
---
![Gilberto Martinez](https://gitlab.com/NotGil/notgil.gitlab.io/raw/master/profile.jpg "Me")




**What did you do this past week?**


This past week was more focused on establishing a good schedule. I found the fastest way to class. I am going to the gym regularly.  I figured out how I'm going to eat throughout the day. And I'm staying ahead of assignments. My roommates and I did try and adopt a dog this week, but it was hard to find a doggo that fit with our living situation. 

**What's in your way?**


Nothing is in my way. I like the way my life is going. I'm staying on top of things, and I'm having lots of fun. On the doggo front, we live in an apartment and have a cat, which limits who we can bring home. 


**What will you do next week?**


Next week I will try and maintain the schedule.  I will work on my sleeping habits, but I have low expectations.  I usually get only 6 hours of sleep when I need 9. 


**What was your experience of learning the basics of Python?**


It's not my first time using python so much of it I already knew. Some of the "Zen of Python" that we learned was new to me. For example, I didn't know += does not return a value because in python the idea is it can modify a value or return a value but not both. 	

**What's your pick-of-the-week or tip-of-the-week?**


In your git config set autocrlf to "input". It will check out code Linux style and check it in Linux style. I was having issues with running Collatz on a Windows Docker container because the line endings were CRLF instead of LF.  Windows use CR LF to end lines, Unix uses LF, and that can cause problems. 
	Also warning, after I installed Docker CE for Windows, my PC started freezing consistently, and after I uninstalled, it stopped freezing.  I initially thought my RAM died on me and spent an hour running memtest. Then went on a long checklist of stuff it could be before I remembered about recently installing Docker. So if you're having those issues try uninstalling Docker. 